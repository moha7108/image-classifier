##########################
# Run the Neural Network #
##########################
import torch
import matplotlib.pyplot as plt
import seaborn as sb
import pandas as pd
import predict_args
from load_model import load_checkpoint
import load_img
import run_model
import json

#get command line parameters and parse
predict_parameters = predict_args.get_args()

print(f'\n{predict_parameters}\n')

#toggle GPU usage for computations
device = torch.device("cuda" if predict_parameters.gpu else "cpu")
print(f'\nThe model will run calculations on the {device}\n')


#load the key for number to name json file
with open(predict_parameters.category_names, 'r') as f:
    cat_to_name = json.load(f)
      
#load model from checkpoint state, process the test image, and run the model to perform inference
model = load_checkpoint(predict_parameters.load_checkpoint, device)
processed_img = load_img.process_image(predict_parameters.img_file)
probs, classes = run_model.predict(processed_img, model,predict_parameters.top_k, device)



plant_name = [] # initiate list of plant names based on keys

#generater plant names based on json dictionary
for key in classes[0]:
    plant_name.append(cat_to_name[str(key)])

results = pd.Series(data = probs[0], index = plant_name)

print(f'\n........Done!\n\nTop {predict_parameters.top_k} results and their probabilities: \n\n')
print(results)



## not necessary for the project
## plot image of input
# y_pos = range(len(plant_name)) #y-position for plotting

# load_img.imshow(processed_img, ax=None, title=None)

# #plot top k images based on our model 
# plt.barh(y_pos, probs[0])
# plt.yticks(y_pos, plant_name)
# plt.xlabel('Probabilities')
# plt.show()
############################
# Train the Neural Network #
############################

import train_args
import predict_args
import torch
from torch import nn
from torchvision import datasets, transforms, models
from torch import optim
import torch.nn.functional as F
from workspace_utils import active_session
from initiate_model import get_model
from load_data import get_data


training_parameters = train_args.get_args()
print (training_parameters)


trainloader, validloader, testloader = get_data(training_parameters.data_dir)

device = torch.device("cuda" if training_parameters.gpu else "cpu")
print(f'The model will train on the {device}')

model = get_model(training_parameters.arch, training_parameters.hidden_units,device)
criterion = nn.NLLLoss()
optimizer = optim.SGD(model.classifier.parameters(), lr=training_parameters.learning_rate)

epochs = training_parameters.epochs
batch_number = 0
steps = 0
running_loss = 0
print_every = 5

with active_session():
    for epoch in range(epochs):
        for inputs, labels in trainloader:
            steps += 1
            inputs = inputs.to(device)
            labels = labels.to(device)

            optimizer.zero_grad()

            output = model.forward(inputs)
            loss = criterion(output, labels)
            loss.backward()
            optimizer.step()

            running_loss += loss.item()

            if steps % print_every == 0:
                batch_number += labels.shape[0]
                valid_loss = 0
                accuracy = 0
                model.eval()
                with torch.no_grad():
                    for inputs, labels in validloader:
                        inputs= inputs.to(device)
                        labels= labels.to(device)
                        output = model.forward(inputs)
                        batch_loss = criterion(output, labels)

                        valid_loss += batch_loss.item()

                        ps = torch.exp(output)
                        top_p, top_class = ps.topk(1, dim=1)
                        equals = top_class == labels.view(*top_class.shape)
                        accuracy += torch.mean(equals.type(torch.FloatTensor)).item()

                print(f'Epoch: {epoch+1} / {epochs}\n'
                      f'images trained: {batch_number}\n'
                      f'Train Error: {running_loss/print_every:.3f}\n'
                      f'validation Error: {valid_loss/len(validloader):.3f}\n'
                      f'validation accuracy: {accuracy/len(validloader):.3f}\n')
                running_loss = 0
                model.train()
                
                
checkpoint = {'state_dict': model.state_dict(),
              'epoch_number': epoch,
              'optimizer': optimizer.state_dict(),
              'classifier': model.classifier,
              'error_function': criterion,
              'architecture': training_parameters.arch,
              'hidden_units': training_parameters.hidden_units
             }

torch.save(checkpoint, training_parameters.save)

print('\nTraining is now done, your check point file can be found here: ' + training_parameters.save )
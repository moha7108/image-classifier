# Image Classifier

An image classifier built with PyTorch, that can be called in the command line.

This project can take a train an image classifier on a dateset, then predict images using the trained model. This include flower image classification example.

This project was developed in fulfillment  of the requirements of the Udacity AI Programminng with Python Nanodegree Program



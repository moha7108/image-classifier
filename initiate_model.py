#####################
# Iniate the Model  #
#####################

import torch
from torch import nn
from torch import optim
from torchvision import datasets, transforms, models


def get_model(model_arch, hidden_units,device):

    
    resnet18 = models.resnet18(pretrained=True)
    alexnet = models.alexnet(pretrained=True)
    vgg16 = models.vgg16(pretrained=True)

    
    model_dict = {'resnet18': resnet18, 'alexnet': alexnet, 'vgg16': vgg16}
    
    
    model = model_dict[model_arch]
    

    for param in model.parameters():
        param.requires_grad = False

    model.classifier = nn.Sequential(nn.Linear(25088, hidden_units),
                                     nn.ReLU(),
                                     nn.Dropout(0.2),
                                     nn.Linear(hidden_units, hidden_units),
                                     nn.ReLU(),
                                     nn.Dropout(0.2),
                                     nn.Linear(hidden_units, 102),
                                     nn.LogSoftmax(dim=1))


    return model.to(device);
